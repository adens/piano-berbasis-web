//loading page
$(window).load(function(){
    $( ".loader" ).delay(800).fadeOut(400);  
    $( ".home" ).fadeIn(400);
});
//fungsi onkeydown yaitu untuk menghandle keyboard komputer pada saat ditekan
document.onkeydown = function(e) {
    switch (e.keyCode) {

	//Untuk Tuts Putih
	case 90: //code ascii tombol z di keyboard komputer
		tekan('1');
	break;
	case 88: //code ascii tombol x di keyboard komputer
		tekan('2');
	break;
	case 67: //code ascii tombol c di keyboard komputer
		tekan('3');
	break;
	case 86: //code ascii tombol v di keyboard komputer
		tekan('4');
	break;
	case 66: //code ascii tombol b di keyboard komputer
		tekan('5');
	break;
	case 78: //code ascii tombol n di keyboard komputer
		tekan('6');
	break;
	case 77: //code ascii tombol m di keyboard komputer
		tekan('7');
	break;
	case 188: //code ascii tombol , di keyboard komputer
		tekan('8');
	break;

	//Untuk Tuts Hitam
	case 83: //code ascii tombol s di keyboard komputer
		tekan('1kres');
	break;
	case 68: //code ascii tombol d di keyboard komputer
		tekan('2kres');
	break;
	case 71: //code ascii tombol g di keyboard komputer
		tekan('4kres');
	break;
	case 72: //code ascii tombol h di keyboard komputer
		tekan('5kres');
	break;
	case 74: //code ascii tombol j di keyboard komputer
		tekan('6kres');
	break;
	}
};

//fungsi ini akan dijalankan pada saat cursor (jari) menekan tuts piano
//tuts putih
$('.1').click(function(){
	tekan('1');
});
$('.2').click(function(){
	tekan('2');
});
$('.3').click(function(){
	tekan('3');
});
$('.4').click(function(){
	tekan('4');
});
$('.5').click(function(){
	tekan('5');
});
$('.6').click(function(){
	tekan('6');
});
$('.7').click(function(){
	tekan('7');
});
$('.8').click(function(){
	tekan('8');
});

//tuts hitam
$('.1kres').click(function(){
	tekan('1kres');
});
$('.2kres').click(function(){
	tekan('2kres');
});
$('.4kres').click(function(){
	tekan('4kres');
});
$('.5kres').click(function(){
	tekan('5kres');
});
$('.6kres').click(function(){
	tekan('6kres');
});

//pada saat ditekan maka akan menjalankan fungsi ini
function tekan(tuts){
	$('#'+tuts).html('<audio controls autoplay  src="assets/audio/'+tuts+'.ogg" class=""></audio>');
	warna(tuts);
}

//fungsi ini untuk mengubah warna tuts menjadi kuning pada saat ditekan
function warna(tuts){
	$('.'+tuts).addClass('kuning');
	setTimeout(function(){
		$('.'+tuts).removeClass('kuning');
	},100);
}